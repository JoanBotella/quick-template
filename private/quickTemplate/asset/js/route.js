
const route = function ()
{
	const
		page = document.querySelector('[data-page]'),
		pageCode = page.getAttribute('data-page')
	;
	if (typeof pageSetuppers[pageCode] === 'function')
	{
		pageSetuppers[pageCode]();
	}
}
