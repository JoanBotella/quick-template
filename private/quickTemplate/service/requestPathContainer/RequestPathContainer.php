<?php
declare(strict_types=1);

namespace quickTemplate\service\requestPathContainer;

use quickTemplate\service\configuration\Configuration;

final class RequestPathContainer
{

	private static string $requestPath;

	public static function get():string
	{
		if (!isset(self::$requestPath))
		{
			self::$requestPath = self::buildRequestPath();
		}
		return self::$requestPath;
	}

		private static function buildRequestPath():string
		{
			return substr(
				$_SERVER['REQUEST_URI'],
				strlen(Configuration::getBaseRequestUri())
			);
		}

}