<?php
declare(strict_types=1);

use quickTemplate\page\home\service\urlBuilder\HomeUrlBuilder;
use quickTemplate\service\configuration\Configuration;

?><!DOCTYPE html>
<html lang="<?= $context['languageCode'] ?>" data-page="<?= $context['pageCode'] ?>" class="page-<?= $context['pageCode'] ?>">
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<base href="<?= Configuration::getBaseRequestUri() ?>" />

	<link rel="icon" href="asset/img/favicon.png" type="image/png" />

	<title><?= $context['pageTitle'] ?> | Quick</title>

	<link href="asset/css/style.css" rel="stylesheet" media="screen" />
</head>
<body>

	<noscript>
		<p>You will need JavaScript to play with me!</p>
	</noscript>

	<header></header>

	<nav>
		<ul>
			<li><a href="<?= HomeUrlBuilder::build() ?>">Home</a></li>
		</ul>
	</nav>

	<main>
		<?= $context['mainContent'] ?>
	</main>

	<footer></footer>

	<script src="asset/js/script.js"></script>
</body>
</html>