<?php
declare(strict_types=1);

namespace quickTemplate\page\home\service\widget\mainContent;

use quickTemplate\service\templater\Templater;

final class HomeMainContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}