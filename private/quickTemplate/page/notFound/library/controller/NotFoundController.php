<?php
declare(strict_types=1);

namespace quickTemplate\page\notFound\library\controller;

use quickTemplate\library\controller\ControllerAbs;
use quickTemplate\page\notFound\service\widget\mainContent\NotFoundMainContentWidget;
use quickTemplate\service\widget\layout\LayoutWidget;

final class NotFoundController extends ControllerAbs
{

	protected function respond():void
	{
		http_response_code(404);

		$context = [
			'languageCode' => 'en',
			'pageCode' => 'not_found',
			'pageTitle' => 'Not Found',
		];

		$context['mainContent'] = NotFoundMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}