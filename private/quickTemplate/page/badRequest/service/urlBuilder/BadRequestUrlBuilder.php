<?php
declare(strict_types=1);

namespace quickTemplate\page\badRequest\service\urlBuilder;

final class BadRequestUrlBuilder
{

	public static function build():string
	{
		return 'badRequest';
	}

}