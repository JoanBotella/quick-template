<?php
declare(strict_types=1);

namespace quickTemplate\page\internalServerError\library\controller;

use quickTemplate\library\controller\ControllerAbs;
use quickTemplate\page\internalServerError\service\widget\mainContent\InternalServerErrorMainContentWidget;
use quickTemplate\service\widget\layout\LayoutWidget;

final class InternalServerErrorController extends ControllerAbs
{

	protected function respond():void
	{
		http_response_code(500);

		$context = [
			'languageCode' => 'en',
			'pageCode' => 'internal_server_error',
			'pageTitle' => 'Internal Server Error',
		];

		$context['mainContent'] = InternalServerErrorMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}